package com.addcel.usuarios.model.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.usuarios.model.vo.AbstractLabelValue;
import com.addcel.usuarios.model.vo.AvisosVO;
import com.addcel.usuarios.model.vo.CiudadesVO;
import com.addcel.usuarios.model.vo.RequestUrlAplicacionModuloVO;
import com.addcel.usuarios.model.vo.ServiciosVO;
import com.addcel.usuarios.model.vo.TiposServicios;
import com.addcel.usuarios.model.vo.UsuariosUsa;

public interface UsuariosServiciosMapper {
	
        public String guardarUsuario(UsuariosUsa usuario);
	
}
