package com.addcel.usuarios.services;

import com.addcel.usuarios.model.mapper.UsuariosServiciosMapper;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.addcel.usuarios.model.vo.AbstractVO;
import com.addcel.usuarios.utils.Constantes;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import com.addcel.usuarios.model.vo.TUsuarios;

@Service
public class UsuarioService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioService.class);
	
	private static final Gson GSON = new Gson();
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat(Constantes.FORMATO_FECHA_ENCRIPT);
	
        @Autowired
	private UsuariosServiciosMapper mapper;
        
	public String guardarUsuario(String json) {
		AbstractVO res = null;
                TUsuarios usu= new TUsuarios();
                usu = GSON.fromJson(json, TUsuarios.class);
                LOGGER.info("validando y seteando correctamente datos enviados . . . ");
                    if(usu.getUsrFechaNac() == null){
                        LOGGER.info("entro FechaNac");
                        usu.setUsrFechaNac("00-00-0000");
                    }  
                    if(usu.getUsrApellido() == null){
                        LOGGER.info("entro UsrApellido");
                        usu.setUsrApellido("");
                    }  
                    if(usu.getUsrDireccion() == null){
                        LOGGER.info("entro UsrDireccion");
                        usu.setUsrDireccion("");
                    }
                    if(usu.getNewPassword() == null){
                        LOGGER.info("entro NewPassword");
                        usu.setNewPassword("");
                    }
                    if(usu.getOperador() == 0){
                        usu.setOperador(0);
                        LOGGER.info("entro operador");
                    }
                    if(usu.getIdBanco() == null){
                        LOGGER.info("entro IdBanco");
                        usu.setIdBanco(0);
                    }
                    if(usu.getIdTipoTarjeta() == null){
                        LOGGER.info("entro IdTipoTarjeta");
                        usu.setIdTipoTarjeta(0);
                    }
                    if(usu.getIdProveedor() == null){
                        LOGGER.info("entro IdProveedor");
                        usu.setIdProveedor(0);
                    }
                    if(usu.getIdUsrStatus() == null){
                        LOGGER.info("entro IdUsrStatus");
                        usu.setIdUsrStatus(99);
                    }
                    if(usu.getCedula() == null){
                        LOGGER.info("entro Cedula");
                        usu.setCedula("");
                    }
                    if(usu.getTipoCedula() == null){
                        LOGGER.info("entro TipoCedula");
                        usu.setTipoCedula(0);
                    }
                    if(usu.getRecibirSMS() == null){
                        LOGGER.info("entro RecibirSMS");
                        usu.setRecibirSMS(0);
                    }
                    if(usu.getIdPais() == null){
                        LOGGER.info("entro IdPais");
                        usu.setIdPais(0);
                    }
                    if(usu.getGemalto() == null){
                        LOGGER.info("entro Gemalto");
                        usu.setGemalto(0);
                    }
                    if(usu.geteMail() == null){
                        LOGGER.info("entro eMail");
                        usu.seteMail("");
                    }
                    if(usu.getImei() == null){
                        LOGGER.info("entro Imei");
                        usu.setImei("");
                    }
                    if(usu.getTipo() == null){
                        LOGGER.info("entro Tipo");
                        usu.setTipo("");
                    }
                    if(usu.getSoftware() == null){
                        LOGGER.info("entro Software");
                        usu.setSoftware("");
                    }
                    if(usu.getModelo() == null){
                        LOGGER.info("entro Modelo");
                        usu.setModelo("");
                    }
                    if(usu.getWkey() == null){
                        LOGGER.info("entro Wkey");
                        usu.setWkey("");
                    }
                    if(usu.getTelefonoOriginal() == null){
                        LOGGER.info("entro TelefonoOriginal");
                        usu.setTelefonoOriginal("");
                    }
                    if(usu.getUsrMaterno() == null){
                        LOGGER.info("entro UsrMaterno");
                        usu.setUsrMaterno("");
                    }
                    if(usu.getUsrSexo() == null){
                        LOGGER.info("entro UsrSexo");
                        usu.setUsrSexo("");
                    }
                    if(usu.getUsrTelCasa() == null){
                        LOGGER.info("entro UsrTelCasa");
                        usu.setUsrTelCasa("");
                    }
                    if(usu.getUsrTelOficina() == null){
                        LOGGER.info("entro UsrTelOficina");
                        usu.setUsrTelOficina("");
                    }
                    if(usu.getUsrIdEstado() == null){
                        LOGGER.info("entro UsrIdEstado");
                        usu.setUsrIdEstado(0);
                    }
                    if(usu.getUsrCiudad() == null){
                        LOGGER.info("entro UsrCiudad");
                        usu.setUsrCiudad("");
                    }
                    if(usu.getUsrCalle() == null){
                        LOGGER.info("entro UsrCalle");
                        usu.setUsrCalle("");
                    }
                    if(usu.getUsrNumExt() == null){
                        LOGGER.info("entro UsrNumExt");
                        usu.setUsrNumExt(0);
                    }
                    if(usu.getUsrNumInterior() == null){
                        LOGGER.info("entro UsrNumInterior");
                        usu.setUsrNumInterior("");
                    }
                    if(usu.getUsrColonia() == null){
                        LOGGER.info("entro UsrColonia");
                        usu.setUsrColonia("");
                    }
                    if(usu.getUsrCp() == null){
                        LOGGER.info("entro UsrCp");
                        usu.setUsrCp("");
                    }
                    if(usu.getUsrDomAmex() == null){
                        LOGGER.info("entro UsrDomAmex");
                        usu.setUsrDomAmex("");
                    }
                    if(usu.getUsrTerminos() == null){
                        LOGGER.info("entro UsrTerminos");
                        usu.setUsrTerminos("");
                    }
                    if(usu.getNumExtStr() == null){
                        LOGGER.info("entro NumExtStr");
                        usu.setNumExtStr("");
                    }
                    if(usu.getIdIngo() == null){
                        LOGGER.info("entro IdIngo");
                        usu.setIdIngo("");
                    }
                    if(usu.getUsrNss() == null){
                        LOGGER.info("entro UsrNss");
                        usu.setUsrNss("");
                    }
                LOGGER.info("guardarUsuario");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
	         try {
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
                 json = GSON.toJson(usu, TUsuarios.class);
                 LOGGER.info("json convertido desppues de los seteos"+json);
		try {
			 json= binding.guardarUsuario(json);
                         System.out.println("Respuesta guardarUsuario :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error guardarUsuario: " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException guardarUsuario: " + re);
		}
                catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(10);
			res.setMensajeError(Constantes.ERROR_INESPERADO_ALTA_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ALTA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		} finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
                
		return json;
	}

	public String actualizarUsuario(String json) {
		AbstractVO res = null;
                TUsuarios usu= new TUsuarios();
                usu = GSON.fromJson(json, TUsuarios.class);
                LOGGER.info("validando y seteando correctamente datos enviados . . . ");
                
                    if(usu.getOperador() == 0){
                        usu.setOperador(0);
                        //LOGGER.info("entro operador");
                    }
                    if(usu.getIdBanco() == null){
                        usu.setIdBanco(0);
                    }
                    if(usu.getIdTipoTarjeta() == null){
                        usu.setIdTipoTarjeta(0);
                    }
                    if(usu.getIdProveedor() == null){
                        usu.setIdProveedor(0);
                    }
                    if(usu.getIdUsrStatus() == null){
                        usu.setIdUsrStatus(0);
                    }
                    if(usu.getCedula() == null){
                        usu.setCedula("");
                    }
                    if(usu.getTipoCedula() == null){
                        usu.setTipoCedula(0);
                    }
                    if(usu.getRecibirSMS() == null){
                        usu.setRecibirSMS(0);
                    }
                    if(usu.getIdPais() == null){
                        usu.setIdPais(0);
                    }
                    if(usu.getGemalto() == null){
                        usu.setGemalto(0);
                    }
                    if(usu.geteMail() == null){
                        usu.seteMail("");
                    }
                    if(usu.getImei() == null){
                        usu.setImei("");
                    }
                    if(usu.getTipo() == null){
                        usu.setTipo("");
                    }
                    if(usu.getSoftware() == null){
                        usu.setSoftware("");
                    }
                    if(usu.getModelo() == null){
                        usu.setModelo("");
                    }
                    if(usu.getWkey() == null){
                        usu.setWkey("");
                    }
                    if(usu.getTelefonoOriginal() == null){
                        usu.setTelefonoOriginal("");
                    }
                    if(usu.getUsrMaterno() == null){
                        usu.setUsrMaterno("");
                    }
                    if(usu.getUsrSexo() == null){
                        usu.setUsrSexo("");
                    }
                    if(usu.getUsrTelCasa() == null){
                        usu.setUsrTelCasa("");
                    }
                    if(usu.getUsrTelOficina() == null){
                        usu.setUsrTelOficina("");
                    }
                    if(usu.getUsrIdEstado() == null){
                        usu.setUsrIdEstado(0);
                    }
                    if(usu.getUsrCiudad() == null){
                        usu.setUsrCiudad("");
                    }
                    if(usu.getUsrCalle() == null){
                        usu.setUsrCalle("");
                    }
                    if(usu.getUsrNumExt() == null){
                        usu.setUsrNumExt(0);
                    }
                    if(usu.getUsrNumInterior() == null){
                        usu.setUsrNumInterior("");
                    }
                    if(usu.getUsrColonia() == null){
                        usu.setUsrColonia("");
                    }
                    if(usu.getUsrCp() == null){
                        usu.setUsrCp("");
                    }
                    if(usu.getUsrDomAmex() == null){
                        usu.setUsrDomAmex("");
                    }
                    if(usu.getUsrTerminos() == null){
                        usu.setUsrTerminos("");
                    }
                    if(usu.getNumExtStr() == null){
                        usu.setNumExtStr("");
                    }
                    if(usu.getIdIngo() == null){
                        usu.setIdIngo("");
                    }
                    if(usu.getUsrNss() == null){
                        usu.setUsrNss("");
                    }
                LOGGER.info("actualizarUsuario");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
                 try {
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
		try {
                      json= binding.actualizarUsuario(json);
                      System.out.println("Respuesta actualizarUsuario :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error actualizarUsuario: " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException actualizarUsuario: " + re);
		} catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(10);
			res.setMensajeError(Constantes.ERROR_INESPERADO_ACTUALIZANDO_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ACTUALIZA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		} finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
        
        
        public String actualizarUsuarioUsu(String json) {
		AbstractVO res = null;
                TUsuarios usu= new TUsuarios();
                usu = GSON.fromJson(json, TUsuarios.class);
                LOGGER.info("validando y seteando correctamente datos enviados . . . ");
                
                    if(usu.getOperador() == 0){
                        usu.setOperador(0);
                        //LOGGER.info("entro operador");
                    }
                    if(usu.getIdBanco() == null){
                        usu.setIdBanco(0);
                    }
                    if(usu.getIdTipoTarjeta() == null){
                        usu.setIdTipoTarjeta(0);
                    }
                    if(usu.getIdProveedor() == null){
                        usu.setIdProveedor(0);
                    }
                    if(usu.getIdUsrStatus() == null){
                        usu.setIdUsrStatus(0);
                    }
                    if(usu.getCedula() == null){
                        usu.setCedula("");
                    }
                    if(usu.getTipoCedula() == null){
                        usu.setTipoCedula(0);
                    }
                    if(usu.getRecibirSMS() == null){
                        usu.setRecibirSMS(0);
                    }
                    if(usu.getIdPais() == null){
                        usu.setIdPais(0);
                    }
                    if(usu.getGemalto() == null){
                        usu.setGemalto(0);
                    }
                    if(usu.geteMail() == null){
                        usu.seteMail("");
                    }
                    if(usu.getImei() == null){
                        usu.setImei("");
                    }
                    if(usu.getTipo() == null){
                        usu.setTipo("");
                    }
                    if(usu.getSoftware() == null){
                        usu.setSoftware("");
                    }
                    if(usu.getModelo() == null){
                        usu.setModelo("");
                    }
                    if(usu.getWkey() == null){
                        usu.setWkey("");
                    }
                    if(usu.getTelefonoOriginal() == null){
                        usu.setTelefonoOriginal("");
                    }
                    if(usu.getUsrMaterno() == null){
                        usu.setUsrMaterno("");
                    }
                    if(usu.getUsrSexo() == null){
                        usu.setUsrSexo("");
                    }
                    if(usu.getUsrTelCasa() == null){
                        usu.setUsrTelCasa("");
                    }
                    if(usu.getUsrTelOficina() == null){
                        usu.setUsrTelOficina("");
                    }
                    if(usu.getUsrIdEstado() == null){
                        usu.setUsrIdEstado(0);
                    }
                    if(usu.getUsrCiudad() == null){
                        usu.setUsrCiudad("");
                    }
                    if(usu.getUsrCalle() == null){
                        usu.setUsrCalle("");
                    }
                    if(usu.getUsrNumExt() == null){
                        usu.setUsrNumExt(0);
                    }
                    if(usu.getUsrNumInterior() == null){
                        usu.setUsrNumInterior("");
                    }
                    if(usu.getUsrColonia() == null){
                        usu.setUsrColonia("");
                    }
                    if(usu.getUsrCp() == null){
                        usu.setUsrCp("");
                    }
                    if(usu.getUsrDomAmex() == null){
                        usu.setUsrDomAmex("");
                    }
                    if(usu.getUsrTerminos() == null){
                        usu.setUsrTerminos("");
                    }
                    if(usu.getNumExtStr() == null){
                        usu.setNumExtStr("");
                    }
                    if(usu.getIdIngo() == null){
                        usu.setIdIngo("");
                    }
                    if(usu.getUsrNss() == null){
                        usu.setUsrNss("");
                    }
                LOGGER.info("actualizarUsuarioUsu");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
                 try {
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
		try {
                      json= binding.actualizarUsrLogin(json);
                      System.out.println("Respuesta actualizarUsuarioUsu :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error actualizarUsuarioUsu: " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException actualizarUsuarioUsu: " + re);
		} catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(10);
			res.setMensajeError(Constantes.ERROR_INESPERADO_ACTUALIZANDO_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ACTUALIZA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		} finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
        /*se actualiza nombre de usuario*/
        public String actualizarNombreUsu(String json) {
		AbstractVO res = null;
                TUsuarios usu= new TUsuarios();
                usu = GSON.fromJson(json, TUsuarios.class);
                LOGGER.info("validando y seteando correctamente datos enviados . . . ");
                
                    if(usu.getOperador() == 0){
                        usu.setOperador(0);
                        //LOGGER.info("entro operador");
                    }
                    if(usu.getIdBanco() == null){
                        usu.setIdBanco(0);
                    }
                    if(usu.getIdTipoTarjeta() == null){
                        usu.setIdTipoTarjeta(0);
                    }
                    if(usu.getIdProveedor() == null){
                        usu.setIdProveedor(0);
                    }
                    if(usu.getIdUsrStatus() == null){
                        usu.setIdUsrStatus(0);
                    }
                    if(usu.getCedula() == null){
                        usu.setCedula("");
                    }
                    if(usu.getTipoCedula() == null){
                        usu.setTipoCedula(0);
                    }
                    if(usu.getRecibirSMS() == null){
                        usu.setRecibirSMS(0);
                    }
                    if(usu.getIdPais() == null){
                        usu.setIdPais(0);
                    }
                    if(usu.getGemalto() == null){
                        usu.setGemalto(0);
                    }
                    if(usu.geteMail() == null){
                        usu.seteMail("");
                    }
                    if(usu.getImei() == null){
                        usu.setImei("");
                    }
                    if(usu.getTipo() == null){
                        usu.setTipo("");
                    }
                    if(usu.getSoftware() == null){
                        usu.setSoftware("");
                    }
                    if(usu.getModelo() == null){
                        usu.setModelo("");
                    }
                    if(usu.getWkey() == null){
                        usu.setWkey("");
                    }
                    if(usu.getTelefonoOriginal() == null){
                        usu.setTelefonoOriginal("");
                    }
                    if(usu.getUsrMaterno() == null){
                        usu.setUsrMaterno("");
                    }
                    if(usu.getUsrSexo() == null){
                        usu.setUsrSexo("");
                    }
                    if(usu.getUsrTelCasa() == null){
                        usu.setUsrTelCasa("");
                    }
                    if(usu.getUsrTelOficina() == null){
                        usu.setUsrTelOficina("");
                    }
                    if(usu.getUsrIdEstado() == null){
                        usu.setUsrIdEstado(0);
                    }
                    if(usu.getUsrCiudad() == null){
                        usu.setUsrCiudad("");
                    }
                    if(usu.getUsrCalle() == null){
                        usu.setUsrCalle("");
                    }
                    if(usu.getUsrNumExt() == null){
                        usu.setUsrNumExt(0);
                    }
                    if(usu.getUsrNumInterior() == null){
                        usu.setUsrNumInterior("");
                    }
                    if(usu.getUsrColonia() == null){
                        usu.setUsrColonia("");
                    }
                    if(usu.getUsrCp() == null){
                        usu.setUsrCp("");
                    }
                    if(usu.getUsrDomAmex() == null){
                        usu.setUsrDomAmex("");
                    }
                    if(usu.getUsrTerminos() == null){
                        usu.setUsrTerminos("");
                    }
                    if(usu.getNumExtStr() == null){
                        usu.setNumExtStr("");
                    }
                    if(usu.getIdIngo() == null){
                        usu.setIdIngo("");
                    }
                    if(usu.getUsrNss() == null){
                        usu.setUsrNss("");
                    }
                LOGGER.info("actualizarNombreUsuario");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
                 try {
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
		try {
                      json= binding.actualizarNombreUsu(json);
                      System.out.println("Respuesta actualizarNombreUsuario :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error actualizarNombreUsuario: " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException actualizarNombreUsuario: " + re);
		} catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(10);
			res.setMensajeError(Constantes.ERROR_INESPERADO_ACTUALIZANDO_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ACTUALIZA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		} finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
        
        /*se actualiza nombre de usuario*/
        public String actualizarPaisUsu(String json) {
		AbstractVO res = null;
                TUsuarios usu= new TUsuarios();
                usu = GSON.fromJson(json, TUsuarios.class);
                LOGGER.info("validando y seteando correctamente datos enviados . . . ");
                
                    if(usu.getOperador() == 0){
                        usu.setOperador(0);
                        //LOGGER.info("entro operador");
                    }
                    if(usu.getIdBanco() == null){
                        usu.setIdBanco(0);
                    }
                    if(usu.getIdTipoTarjeta() == null){
                        usu.setIdTipoTarjeta(0);
                    }
                    if(usu.getIdProveedor() == null){
                        usu.setIdProveedor(0);
                    }
                    if(usu.getIdUsrStatus() == null){
                        usu.setIdUsrStatus(0);
                    }
                    if(usu.getCedula() == null){
                        usu.setCedula("");
                    }
                    if(usu.getTipoCedula() == null){
                        usu.setTipoCedula(0);
                    }
                    if(usu.getRecibirSMS() == null){
                        usu.setRecibirSMS(0);
                    }
                    if(usu.getIdPais() == null){
                        usu.setIdPais(0);
                    }
                    if(usu.getGemalto() == null){
                        usu.setGemalto(0);
                    }
                    if(usu.geteMail() == null){
                        usu.seteMail("");
                    }
                    if(usu.getImei() == null){
                        usu.setImei("");
                    }
                    if(usu.getTipo() == null){
                        usu.setTipo("");
                    }
                    if(usu.getSoftware() == null){
                        usu.setSoftware("");
                    }
                    if(usu.getModelo() == null){
                        usu.setModelo("");
                    }
                    if(usu.getWkey() == null){
                        usu.setWkey("");
                    }
                    if(usu.getTelefonoOriginal() == null){
                        usu.setTelefonoOriginal("");
                    }
                    if(usu.getUsrMaterno() == null){
                        usu.setUsrMaterno("");
                    }
                    if(usu.getUsrSexo() == null){
                        usu.setUsrSexo("");
                    }
                    if(usu.getUsrTelCasa() == null){
                        usu.setUsrTelCasa("");
                    }
                    if(usu.getUsrTelOficina() == null){
                        usu.setUsrTelOficina("");
                    }
                    if(usu.getUsrIdEstado() == null){
                        usu.setUsrIdEstado(0);
                    }
                    if(usu.getUsrCiudad() == null){
                        usu.setUsrCiudad("");
                    }
                    if(usu.getUsrCalle() == null){
                        usu.setUsrCalle("");
                    }
                    if(usu.getUsrNumExt() == null){
                        usu.setUsrNumExt(0);
                    }
                    if(usu.getUsrNumInterior() == null){
                        usu.setUsrNumInterior("");
                    }
                    if(usu.getUsrColonia() == null){
                        usu.setUsrColonia("");
                    }
                    if(usu.getUsrCp() == null){
                        usu.setUsrCp("");
                    }
                    if(usu.getUsrDomAmex() == null){
                        usu.setUsrDomAmex("");
                    }
                    if(usu.getUsrTerminos() == null){
                        usu.setUsrTerminos("");
                    }
                    if(usu.getNumExtStr() == null){
                        usu.setNumExtStr("");
                    }
                    if(usu.getIdIngo() == null){
                        usu.setIdIngo("");
                    }
                    if(usu.getUsrNss() == null){
                        usu.setUsrNss("");
                    }
                LOGGER.info("actualizarPaisUsuario");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
                 try {
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
		try {
                      json= binding.actualizarPaisUsu(json);
                      System.out.println("Respuesta actualizarPaisUsu :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error actualizarPaisUsu: " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException actualizarPaisUsu: " + re);
		} catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(10);
			res.setMensajeError(Constantes.ERROR_INESPERADO_ACTUALIZANDO_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ACTUALIZA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		} finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
        
        
        public String actualizarCorreo(String json) {
		AbstractVO res = null;
                TUsuarios usu= new TUsuarios();
                usu = GSON.fromJson(json, TUsuarios.class);
                LOGGER.info("validando y seteando correctamente datos enviados . . . ");
                
                    if(usu.getOperador() == 0){
                        usu.setOperador(0);
                        //LOGGER.info("entro operador");
                    }
                    if(usu.getIdBanco() == null){
                        usu.setIdBanco(0);
                    }
                    if(usu.getIdTipoTarjeta() == null){
                        usu.setIdTipoTarjeta(0);
                    }
                    if(usu.getIdProveedor() == null){
                        usu.setIdProveedor(0);
                    }
                    if(usu.getIdUsrStatus() == null){
                        usu.setIdUsrStatus(0);
                    }
                    if(usu.getCedula() == null){
                        usu.setCedula("");
                    }
                    if(usu.getTipoCedula() == null){
                        usu.setTipoCedula(0);
                    }
                    if(usu.getRecibirSMS() == null){
                        usu.setRecibirSMS(0);
                    }
                    if(usu.getIdPais() == null){
                        usu.setIdPais(0);
                    }
                    if(usu.getGemalto() == null){
                        usu.setGemalto(0);
                    }
                    if(usu.geteMail() == null){
                        usu.seteMail("");
                    }
                    if(usu.getImei() == null){
                        usu.setImei("");
                    }
                    if(usu.getTipo() == null){
                        usu.setTipo("");
                    }
                    if(usu.getSoftware() == null){
                        usu.setSoftware("");
                    }
                    if(usu.getModelo() == null){
                        usu.setModelo("");
                    }
                    if(usu.getWkey() == null){
                        usu.setWkey("");
                    }
                    if(usu.getTelefonoOriginal() == null){
                        usu.setTelefonoOriginal("");
                    }
                    if(usu.getUsrMaterno() == null){
                        usu.setUsrMaterno("");
                    }
                    if(usu.getUsrSexo() == null){
                        usu.setUsrSexo("");
                    }
                    if(usu.getUsrTelCasa() == null){
                        usu.setUsrTelCasa("");
                    }
                    if(usu.getUsrTelOficina() == null){
                        usu.setUsrTelOficina("");
                    }
                    if(usu.getUsrIdEstado() == null){
                        usu.setUsrIdEstado(0);
                    }
                    if(usu.getUsrCiudad() == null){
                        usu.setUsrCiudad("");
                    }
                    if(usu.getUsrCalle() == null){
                        usu.setUsrCalle("");
                    }
                    if(usu.getUsrNumExt() == null){
                        usu.setUsrNumExt(0);
                    }
                    if(usu.getUsrNumInterior() == null){
                        usu.setUsrNumInterior("");
                    }
                    if(usu.getUsrColonia() == null){
                        usu.setUsrColonia("");
                    }
                    if(usu.getUsrCp() == null){
                        usu.setUsrCp("");
                    }
                    if(usu.getUsrDomAmex() == null){
                        usu.setUsrDomAmex("");
                    }
                    if(usu.getUsrTerminos() == null){
                        usu.setUsrTerminos("");
                    }
                    if(usu.getNumExtStr() == null){
                        usu.setNumExtStr("");
                    }
                    if(usu.getIdIngo() == null){
                        usu.setIdIngo("");
                    }
                    if(usu.getUsrNss() == null){
                        usu.setUsrNss("");
                    }
                LOGGER.info("actualizarCorreo");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
                 try {
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
		try {
                      json= binding.actualizarCorreo(json);
                      System.out.println("Respuesta actualizarCorreo :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error actualizarCorreo: " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException actualizarCorreo: " + re);
		} catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(10);
			res.setMensajeError(Constantes.ERROR_INESPERADO_ACTUALIZANDO_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ACTUALIZA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		} finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}

        public String actualizarTelefono(String json) {
		AbstractVO res = null;
                TUsuarios usu= new TUsuarios();
                usu = GSON.fromJson(json, TUsuarios.class);
                LOGGER.info("validando y seteando correctamente datos enviados . . . ");
                
                    if(usu.getOperador() == 0){
                        usu.setOperador(0);
                        //LOGGER.info("entro operador");
                    }
                    if(usu.getIdBanco() == null){
                        usu.setIdBanco(0);
                    }
                    if(usu.getIdTipoTarjeta() == null){
                        usu.setIdTipoTarjeta(0);
                    }
                    if(usu.getIdProveedor() == null){
                        usu.setIdProveedor(0);
                    }
                    if(usu.getIdUsrStatus() == null){
                        usu.setIdUsrStatus(0);
                    }
                    if(usu.getCedula() == null){
                        usu.setCedula("");
                    }
                    if(usu.getTipoCedula() == null){
                        usu.setTipoCedula(0);
                    }
                    if(usu.getRecibirSMS() == null){
                        usu.setRecibirSMS(0);
                    }
                    if(usu.getIdPais() == null){
                        usu.setIdPais(0);
                    }
                    if(usu.getGemalto() == null){
                        usu.setGemalto(0);
                    }
                    if(usu.geteMail() == null){
                        usu.seteMail("");
                    }
                    if(usu.getImei() == null){
                        usu.setImei("");
                    }
                    if(usu.getTipo() == null){
                        usu.setTipo("");
                    }
                    if(usu.getSoftware() == null){
                        usu.setSoftware("");
                    }
                    if(usu.getModelo() == null){
                        usu.setModelo("");
                    }
                    if(usu.getWkey() == null){
                        usu.setWkey("");
                    }
                    if(usu.getTelefonoOriginal() == null){
                        usu.setTelefonoOriginal("");
                    }
                    if(usu.getUsrMaterno() == null){
                        usu.setUsrMaterno("");
                    }
                    if(usu.getUsrSexo() == null){
                        usu.setUsrSexo("");
                    }
                    if(usu.getUsrTelCasa() == null){
                        usu.setUsrTelCasa("");
                    }
                    if(usu.getUsrTelOficina() == null){
                        usu.setUsrTelOficina("");
                    }
                    if(usu.getUsrIdEstado() == null){
                        usu.setUsrIdEstado(0);
                    }
                    if(usu.getUsrCiudad() == null){
                        usu.setUsrCiudad("");
                    }
                    if(usu.getUsrCalle() == null){
                        usu.setUsrCalle("");
                    }
                    if(usu.getUsrNumExt() == null){
                        usu.setUsrNumExt(0);
                    }
                    if(usu.getUsrNumInterior() == null){
                        usu.setUsrNumInterior("");
                    }
                    if(usu.getUsrColonia() == null){
                        usu.setUsrColonia("");
                    }
                    if(usu.getUsrCp() == null){
                        usu.setUsrCp("");
                    }
                    if(usu.getUsrDomAmex() == null){
                        usu.setUsrDomAmex("");
                    }
                    if(usu.getUsrTerminos() == null){
                        usu.setUsrTerminos("");
                    }
                    if(usu.getNumExtStr() == null){
                        usu.setNumExtStr("");
                    }
                    if(usu.getIdIngo() == null){
                        usu.setIdIngo("");
                    }
                    if(usu.getUsrNss() == null){
                        usu.setUsrNss("");
                    }
                LOGGER.info("actualizarTelefono");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
                 try {
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
		try {
                      json= binding.actualizarTelefono(json);
                      System.out.println("Respuesta actualizarTelefono :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error actualizarTelefono: " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException actualizarTelefono: " + re);
		} catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(10);
			res.setMensajeError(Constantes.ERROR_INESPERADO_ACTUALIZANDO_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ACTUALIZA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		} finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
        
	public String loginUsuario(String json) {
		AbstractVO res = null;
                LOGGER.info("loginUsuario");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
                try
                {
                  //LOGGER.info("entro try 1");
	         try {
                     //LOGGER.info("entro try 2");
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                       // LOGGER.info("despues de binding");
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
                 try
		{
		    //System.out.println("Llego 1 ");
                    // Time out after a minute
		    json= binding.login(json);
		    System.out.println("Respuesta :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error : " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException: " + re);
		}
		}catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(61);
			res.setMensajeError(Constantes.ERROR_LOGIN_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ACTUALIZA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		}finally{
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
	
        
        public String resetPassUsuario(String json) {
		AbstractVO res = null;
                LOGGER.info("resetPassUsuario");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
                try
                {
                  //LOGGER.info("entro try 1");
	         try {
                    // LOGGER.info("entro try 2");
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                        //LOGGER.info("despues de binding");
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
                 try
		{
		    //System.out.println("Llego 1 ");
                    // Time out after a minute
		    json = binding.resetPassword(json);
		    System.out.println("Respuesta :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error : " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException: " + re);
		}
		}catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(61);
			res.setMensajeError(Constantes.ERROR_LOGIN_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ACTUALIZA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		}finally{
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
        
        public String changePassUsuario(String json) {
		AbstractVO res = null;
                LOGGER.info("changePassUsuario");
                com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
                try
                {
                  //LOGGER.info("entro try 1");
	         try {
                    // LOGGER.info("entro try 2");
                        binding = (com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
	                          new com.addcel.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                        //LOGGER.info("despues de binding");
                    } catch (javax.xml.rpc.ServiceException jre) {
                        if (jre.getLinkedCause() != null) {
                            jre.getLinkedCause().printStackTrace();
                        }
                    }
                 try
		{
		    //System.out.println("Llego 1 ");
                    // Time out after a minute
		    json = binding.cambiarPassword(json);
		    System.out.println("Respuesta :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error : " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException: " + re);
		}
		}catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(61);
			res.setMensajeError(Constantes.ERROR_LOGIN_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_ACTUALIZA_USUARIO+"["+json+"]");
			pe.printStackTrace();
		}finally{
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
        
	/*public String borrarUsuario(String json) {
		AbstractVO res = null;
		try {
			json =  AddcelCrypto.decryptSensitive(json);
			LOGGER.info(Constantes.LOG_PROCESO_BORRAR_USUARIO+"["+json+"]");
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
			AddcelUsuariosBridgeWSProxy bridgeWSProxy = new AddcelUsuariosBridgeWSProxy();
			json = bridgeWSProxy.bajaUsuario(json);
			json =  AddcelCrypto.decryptSensitive(json);
		} catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(10);
			res.setMensajeError("Error al registrar usuario");
			LOGGER.error(Constantes.LOG_PROCESO_BORRAR_USUARIO+"["+json+"]");
			pe.printStackTrace();
		} finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}*/
	
}
